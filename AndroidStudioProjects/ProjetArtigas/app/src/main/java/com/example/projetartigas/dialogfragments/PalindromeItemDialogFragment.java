package com.example.projetartigas.dialogfragments;

import android.widget.EditText;

import com.example.projetartigas.R;
import com.example.projetartigas.utilities.Palindrome;

public class PalindromeItemDialogFragment extends InputItemDialogFragment<Palindrome> {
    public PalindromeItemDialogFragment(Integer layoutID) {
        super(layoutID);
    }

    @Override
    public void submitInput() {
        passData(new Palindrome(((EditText) getDialog().findViewById(R.id.palindromeEditText)).getText().toString().trim()));
    }

    @Override
    public void passData(Palindrome data) {
        dataPass.onDataPass(data);
    }
}
