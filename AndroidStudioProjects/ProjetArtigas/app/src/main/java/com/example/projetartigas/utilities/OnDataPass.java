package com.example.projetartigas.utilities;

public interface OnDataPass<T> {
    void onDataPass(T data);
}