package com.example.projetartigas.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.projetartigas.R;
import com.example.projetartigas.dialogfragments.AdminDialogFragment;
import com.example.projetartigas.dialogfragments.PalindromeItemDialogFragment;
import com.example.projetartigas.utilities.FileManager;
import com.example.projetartigas.utilities.OnDataPass;
import com.example.projetartigas.utilities.Palindrome;

import java.util.ArrayList;

public abstract class ListViewActivity extends AppCompatActivity implements OnDataPass<Palindrome> {
    public static final String ITEM_ARRAY_LIST_KEY = "itemArrayListKey";
    public static final String FILENAME_KEY = "filenameKey";

    public ArrayList<Palindrome> itemArrayList;
    public ArrayAdapter<Palindrome> arrayAdapter;
    public FileManager fileManager;
    public String filename;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_items);

        String filenameFromIntent = getIntent().getStringExtra(AdminDialogFragment.FILENAME_FOR_ACTIVITY);
        if (filenameFromIntent == null) {
            Toast.makeText(this, R.string.error_message_no_filename, Toast.LENGTH_SHORT).show();
            android.os.Process.killProcess(android.os.Process.myPid());
        }

        // Different order because if we saved the current ArrayList we want to keep the modifications.
        if (savedInstanceState != null) {
            filename = savedInstanceState.getString(FILENAME_KEY);
            itemArrayList = savedInstanceState.getParcelableArrayList(ITEM_ARRAY_LIST_KEY);
            fileManager = new FileManager(getApplicationContext(), filename);
        }
        else {
            filename = filenameFromIntent;
            fileManager = new FileManager(getApplicationContext(), filename);
            itemArrayList = fileManager.readFile(MainActivity.READ_FILE_FROM_CACHE);
        }

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_checked, itemArrayList);
        ((ListView) findViewById(R.id.listView)).setAdapter(arrayAdapter);
    }

    @Override
    public void onRestart() {
        super.onRestart();
        System.out.println("WHY");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_view_activity, menu);
        return true;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(ITEM_ARRAY_LIST_KEY, itemArrayList);
        outState.putString(FILENAME_KEY, filename);
        super.onSaveInstanceState(outState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        itemArrayList = savedInstanceState.getParcelableArrayList(ITEM_ARRAY_LIST_KEY);
        filename = savedInstanceState.getString(FILENAME_KEY);

    }

    public void refreshArrayAdapter() {
        arrayAdapter.notifyDataSetChanged();
    }


    public void addNewItem(View myVue) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        PalindromeItemDialogFragment palindromeDialogFragment = new PalindromeItemDialogFragment(R.layout.activity_dialog_fragment_input);
        palindromeDialogFragment.show(fragmentManager, "PalindromeDialogFragment");
    }

    public void deleteCheckedItems(MenuItem menuItem) {
        ListView itemListView = findViewById(R.id.listView);
        ArrayList<Integer> checkedTasks = new ArrayList<>();
        SparseBooleanArray checkedTasksPosition = itemListView.getCheckedItemPositions();

        for (Integer i=0; i<itemListView.getCount(); i++)
            if(checkedTasksPosition.get(i))
                checkedTasks.add(i);

        for (int i=checkedTasks.size()-1; i>=0 ; i--)
            itemArrayList.remove((int)checkedTasks.get(i));

        itemListView.clearChoices();
        refreshArrayAdapter();
    }

    public void saveItems(MenuItem menuItem) {
        Boolean ifFileWasSaved = fileManager.saveFile(itemArrayList);
        if (ifFileWasSaved)
            Toast.makeText(this, R.string.file_was_saved, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, R.string.file_was_not_saved, Toast.LENGTH_SHORT).show();
    }

    @Override
    public abstract void onDataPass(Palindrome data);
}