package com.example.projetartigas.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.projetartigas.R;
import com.example.projetartigas.dialogfragments.AboutDialogFragment;
import com.example.projetartigas.dialogfragments.AdminDialogFragment;
import com.example.projetartigas.utilities.FileManager;

import java.text.Normalizer;

public class MainTestActivity extends AppCompatActivity {
    private FileManager fileManager;
    private FileManager notFileManager;
    public static final String CUSTOM_PALINDROMES_FILENAME = "customPalindromes.txt";
    public static final String CUSTOM_NOT_PALINDROMES_FILENAME = "customNotPalindromes.txt";
    public static final Integer READ_FILE_FROM_CACHE = 0;
    public static final Integer READ_FILE_FROM_ASSETS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_test);
        createAndUpdateCustomFiles();
    }

    public void createAndUpdateCustomFiles() {
        fileManager = new FileManager(getApplicationContext(), CUSTOM_PALINDROMES_FILENAME);
        notFileManager = new FileManager(getApplicationContext(), CUSTOM_NOT_PALINDROMES_FILENAME);

        // If the custom files do not exist we create them by copying the content of the default files.
        if (fileManager.readFile(READ_FILE_FROM_CACHE).size() == 0)
            fileManager.saveFile((new FileManager(getApplicationContext(), "palindromes.txt")).readFile(READ_FILE_FROM_ASSETS));

        if (notFileManager.readFile(READ_FILE_FROM_CACHE).size() == 0)
            notFileManager.saveFile((new FileManager(getApplicationContext(), "nonpalindromes.txt")).readFile(READ_FILE_FROM_ASSETS));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_test_activity, menu);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        createAndUpdateCustomFiles();
    }

    public void cleanUI() {
        ((EditText) findViewById(R.id.editTextPalindrome)).setText("");
        ((TextView) findViewById(R.id.textView)).setText("");
        updateProgressBar(findViewById(R.id.progressBar), 0);
    }

    // Put to lowercase, remove accents with Normalizer and remove everything that is not a letter.
    public String lowerizeAndNormalizeString(String input) {
        return Normalizer.normalize(input.toLowerCase(), Normalizer.Form.NFD).replaceAll("\\p{M}", "").replaceAll("[^a-z]", "");
    }

    public void updateProgressBar(ProgressBar progressBar, Integer amout) {
        progressBar.setProgress(amout);
    }

    public void cleanText(View myVue) {
        String editTextPalindrome = ((EditText) findViewById(R.id.editTextPalindrome)).getText().toString();
        ((TextView) findViewById(R.id.textView)).setText(lowerizeAndNormalizeString(editTextPalindrome));
    }

    public void compareTexts(View myVue) {
        String editTextString = ((EditText) findViewById(R.id.editTextPalindrome)).getText().toString();
        String textViewString = ((TextView) findViewById(R.id.textView)).getText().toString();

        if (editTextString == null || editTextString.trim().isEmpty()) {
            Toast.makeText(this, R.string.message_no_text_input, Toast.LENGTH_SHORT).show();
            return;
        }

        if (textViewString == null || textViewString.trim().isEmpty()) {
            Toast.makeText(this, R.string.message_not_cleaned, Toast.LENGTH_SHORT).show();
            return;
        }

        SpannableString spannableString1 = new SpannableString(textViewString);
        SpannableString spannableString2 = new SpannableString(textViewString);

        // Setting a new maximum to make it more accurate.
        ((ProgressBar) findViewById(R.id.progressBar)).setMax(textViewString.length()/2);


        Thread thread = new Thread() {
            @Override
            public void run() {
                Boolean isPalindrome = true;
                for (int i=0; i<textViewString.length()/2; i++) {
                    if (textViewString.charAt(i) == textViewString.charAt(textViewString.length()-1-i)) {
                        Integer endSpannableString1 = (textViewString.length()%2 == 0) ? i+1 : i+2;
                        Integer startSpannableString2 = (textViewString.length()%2 == 0) ? textViewString.length()-1-i : textViewString.length()-2-i;

                        spannableString1.setSpan(new BackgroundColorSpan(Color.GREEN), 0, endSpannableString1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        spannableString1.setSpan(new BackgroundColorSpan(Color.GREEN), startSpannableString2, textViewString.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                        updateTextViewOnThread((findViewById(R.id.textView)), spannableString1);
                        updateProgressBar(findViewById(R.id.progressBar), i+1);
                    }
                    else {
                        spannableString1.setSpan(new BackgroundColorSpan(Color.RED), i, i+1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        spannableString1.setSpan(new BackgroundColorSpan(Color.RED),  textViewString.length()-1-i, textViewString.length()-1-i+1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        updateTextViewOnThread((findViewById(R.id.textView)), spannableString1);
                        isPalindrome = false;
                        break;
                    }

                    try {
                        sleep(50);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                String messageToToast = (isPalindrome) ?
                        getString(R.string.is_palindrome) :
                        getString(R.string.is_not_palindrome);
                runOnUiThread(() -> {
                    Toast.makeText(MainTestActivity.this, messageToToast, Toast.LENGTH_SHORT).show();
                });
            }
        };

        thread.start();
    }

    public void updateTextViewOnThread(TextView textView, SpannableString spannableString) {
        runOnUiThread(() -> {
            textView.setText(spannableString);
        });
    }

    public void getRandomizedPalindrome(MenuItem menuItem) {
        cleanUI();
        switch (menuItem.getItemId()) {
            case R.id.itemRandomPalindrome: {
                ((EditText) findViewById(R.id.editTextPalindrome)).setText(fileManager.getRandomItem().toString());
                break;
            }
            case R.id.itemIsThisPalindrome: {
                ((EditText) findViewById(R.id.editTextPalindrome)).setText(notFileManager.getRandomItem().toString());
                break;
            }
        }
    }
}