package com.example.projetartigas.dialogfragments;

import android.app.AlertDialog;
import android.app.Dialog;
import androidx.fragment.app.DialogFragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;

import com.example.projetartigas.R;
import com.example.projetartigas.utilities.OnDataPass;

public abstract class InputItemDialogFragment<T> extends DialogFragment {
    OnDataPass dataPass;
    Integer layoutID;

    public InputItemDialogFragment(Integer layoutID) {
        this.layoutID = layoutID;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        builder .setView(inflater.inflate(layoutID, null))
                .setMessage(R.string.app_name)
                .setPositiveButton(R.string.button_submit, (dialog, which) -> submitInput())
                .setNegativeButton(R.string.button_cancel, (dialog, which) -> dismiss());

        return builder.create();
    }

    public abstract void submitInput();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPass = (OnDataPass) context;
    }

    public abstract void passData(T data);
}
