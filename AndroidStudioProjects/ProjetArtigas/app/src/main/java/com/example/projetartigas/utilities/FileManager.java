package com.example.projetartigas.utilities;

import android.content.Context;

import com.example.projetartigas.activities.MainActivity;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class FileManager {
    private final Context context;
    private final String filename;
    public ArrayList<Palindrome> cachedItemArrayList;

    public FileManager(Context context, String filename) {
        this.context = context;
        this.filename = filename;
    }

    public ArrayList<Palindrome> readFile(Integer action) {
        cachedItemArrayList = new ArrayList<>();
        try {
            BufferedReader bufferedReader = (action == MainActivity.READ_FILE_FROM_ASSETS) ?
                    new BufferedReader(new InputStreamReader(context.getAssets().open(filename), StandardCharsets.UTF_8)) :
                    new BufferedReader(new InputStreamReader(context.openFileInput(filename), StandardCharsets.UTF_8));

            for (String item; (item = bufferedReader.readLine()) != null ;)
                cachedItemArrayList.add(new Palindrome(item.trim()));

            bufferedReader.close();
        }

        // The FileNotFoundException must be separated to create a file if not exists.
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        catch (Exception e) {
            e.printStackTrace();
        }

        return cachedItemArrayList;
    }

    public Palindrome getRandomItem() {
        return cachedItemArrayList.get(((int) (Math.random() * cachedItemArrayList.size())));
    }

    public Boolean saveFile(ArrayList<Palindrome> updatedItemArrayList) {
        try {
            cachedItemArrayList = (ArrayList<Palindrome>) updatedItemArrayList.clone();

            FileOutputStream fileOutputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);

            for (Integer i=0; i<updatedItemArrayList.size(); i++)
                outputStreamWriter.append(String.format("%s\n", updatedItemArrayList.get(i).toString().trim()));

            outputStreamWriter.close();
            fileOutputStream.close();
            return true;
        }

        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
