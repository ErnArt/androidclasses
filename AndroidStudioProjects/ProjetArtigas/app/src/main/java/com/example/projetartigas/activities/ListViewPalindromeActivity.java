package com.example.projetartigas.activities;

import android.widget.Toast;

import com.example.projetartigas.R;
import com.example.projetartigas.utilities.OnDataPass;
import com.example.projetartigas.utilities.Palindrome;

public class ListViewPalindromeActivity extends ListViewActivity implements OnDataPass<Palindrome> {

    @Override
    public void onDataPass(Palindrome data) {
        if (data.toString().isEmpty() || itemArrayList.contains(data)) {
            Toast.makeText(this, R.string.item_already_exists, Toast.LENGTH_SHORT).show();
            return;
        }
        if (!data.isPalindrome()) {
            Toast.makeText(this, R.string.is_not_palindrome, Toast.LENGTH_SHORT).show();
            return;
        }
        itemArrayList.add(data);
        refreshArrayAdapter();
    }
}