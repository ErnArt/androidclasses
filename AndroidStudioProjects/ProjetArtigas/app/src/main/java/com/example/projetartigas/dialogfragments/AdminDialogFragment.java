package com.example.projetartigas.dialogfragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import com.example.projetartigas.R;
import com.example.projetartigas.activities.ListViewNotPalindromeActivity;
import com.example.projetartigas.activities.ListViewPalindromeActivity;
import com.example.projetartigas.activities.MainActivity;

public class AdminDialogFragment extends DialogFragment {
    public static final String FILENAME_FOR_ACTIVITY = "com.example.projectartigas.FILENAME_FOR_ACTIVITY";

    public String orderSwitchValue;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.activity_dialog_admin, null);
        builder .setView(view)
                .setMessage(R.string.dialog_admin_title)
                .setPositiveButton(R.string.button_confirm, (dialog, which) -> authenticate())
                .setNegativeButton(R.string.button_cancel, (dialog, which) -> AdminDialogFragment.this.getDialog().cancel());

        Switch switchSelectMode = view.findViewById(R.id.switchSelectMode);
        switchSelectMode.setOnClickListener(element -> onSwitchClicked());
        switchSelectMode.setText(R.string.switch_mode_on);
        switchSelectMode.setChecked(true);
        return builder.create();
    }

    public void onSwitchClicked() {
        orderSwitchValue = (String) ((((Switch) getDialog().findViewById(R.id.switchSelectMode)).isChecked()) ?
            getText(R.string.switch_mode_on) :
            getText(R.string.switch_mode_off));
        ((Switch) getDialog().findViewById(R.id.switchSelectMode)).setText(orderSwitchValue);
    }

    public void authenticate() {
        String password = ((EditText) getDialog().findViewById(R.id.editTextPassword)).getText().toString();
        if (!password.equals(getString(R.string.admin_pasword))) {
            Toast.makeText(getActivity(), R.string.dialog_admin_wrong_password, Toast.LENGTH_SHORT).show();
            return;
        }

        Boolean isSwitchChecked = ((Switch) getDialog().findViewById(R.id.switchSelectMode)).isChecked();

        String filename = (isSwitchChecked) ?
                MainActivity.CUSTOM_PALINDROMES_FILENAME :
                MainActivity.CUSTOM_NOT_PALINDROMES_FILENAME;

        Class activityClass = (isSwitchChecked) ?
                ListViewPalindromeActivity.class :
                ListViewNotPalindromeActivity.class;

        Intent intent = new Intent(getActivity(), activityClass);
        intent.putExtra(FILENAME_FOR_ACTIVITY, filename);
        getActivity().startActivity(intent);
    }
}
