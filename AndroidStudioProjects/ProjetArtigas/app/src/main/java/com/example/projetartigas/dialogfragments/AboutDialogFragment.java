package com.example.projetartigas.dialogfragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.fragment.app.DialogFragment;

import com.example.projetartigas.R;

public class AboutDialogFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstaceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        builder .setView(inflater.inflate(R.layout.activity_dialog_about, null))
                .setMessage(R.string.item_about_app)
                .setPositiveButton(R.string.button_confirm, (dialog, which) -> dismiss());

        return builder.create();
    }
}
