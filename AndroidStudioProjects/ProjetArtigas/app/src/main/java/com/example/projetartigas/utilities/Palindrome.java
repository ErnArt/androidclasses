package com.example.projetartigas.utilities;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.Normalizer;

public class Palindrome implements Parcelable {
    public String palindrome;
    
    public Palindrome(String string) {
        palindrome = string;
    }

    protected Palindrome(Parcel in) {
        palindrome = in.readString();
    }

    public static final Creator<Palindrome> CREATOR = new Creator<Palindrome>() {
        @Override
        public Palindrome createFromParcel(Parcel in) {
            return new Palindrome(in);
        }

        @Override
        public Palindrome[] newArray(int size) {
            return new Palindrome[size];
        }
    };

    public String lowerizeAndNormalizeString() {
        return Normalizer.normalize(palindrome.toLowerCase(), Normalizer.Form.NFD).replaceAll("\\p{M}", "").replaceAll("[^a-z]", "");
    }

    public Boolean isPalindrome() {
        String lowerizedAndNormalizedString = lowerizeAndNormalizeString();
        for (Integer i = 0; i<lowerizedAndNormalizedString.length(); i++)
            if (lowerizedAndNormalizedString.charAt(i) != lowerizedAndNormalizedString.charAt(lowerizedAndNormalizedString.length()-1-i))
                return false;
        return true;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null || object.getClass() != this.getClass())
            return false;
        return ((Palindrome) object).palindrome.equals(this.palindrome);
    }

    @Override
    public String toString() {
        return palindrome;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(palindrome);
    }
}
