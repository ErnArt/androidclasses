package com.example.todoartigas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

public class AddTaskDialogFragment extends DialogFragment {
    OnDataPass dataPass;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        builder .setView(inflater.inflate(R.layout.activity_dialog_fragment_input, null))
                .setMessage(R.string.task_title_input_activity)
                .setPositiveButton(R.string.button_submit, (dialog, which) -> submitInput())
                .setNegativeButton(R.string.button_cancel, (dialog, which) -> dismiss());

        return builder.create();
    }

    public void submitInput() {
        passData(((EditText) getDialog().findViewById(R.id.taskEditText)).getText().toString().trim());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPass = (OnDataPass) context;
    }

    public void passData(String data) {
        dataPass.onDataPass(data);
    }
}
