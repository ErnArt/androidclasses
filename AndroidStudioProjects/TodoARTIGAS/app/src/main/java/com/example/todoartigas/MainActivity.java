package com.example.todoartigas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Parcelable;
import android.util.SparseBooleanArray;
import android.view.View;

import androidx.fragment.app.FragmentManager;
import androidx.navigation.ui.AppBarConfiguration;

import com.example.todoartigas.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnDataPass {
    public static final String TASKS_ARRAY_LIST_KEY = "tasksArrayList";
    public static final String FILENAME_KEY = "filenameKey";

    private ArrayList<String> tasksArrayList;

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;

    private FileManager fileManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);

        tasksArrayList = new ArrayList<>();

        fileManager = new FileManager(getApplicationContext(), "tasks.txt");

        if (savedInstanceState == null) {
            ArrayList<String> tasksArrayFromFile = fileManager.readFile();

            if (tasksArrayFromFile.size() == 0)
                generateListView();

            else
                tasksArrayList = tasksArrayFromFile;
        }

        else
            tasksArrayList = (ArrayList<String>) savedInstanceState.getStringArrayList(TASKS_ARRAY_LIST_KEY);

        setArrayListToListView(tasksArrayList, R.id.listView);

    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putStringArrayList(TASKS_ARRAY_LIST_KEY, tasksArrayList);
        super.onSaveInstanceState(outState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        tasksArrayList = savedInstanceState.getStringArrayList(TASKS_ARRAY_LIST_KEY);
    }

    public void addNewTask(View myVue) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        AddTaskDialogFragment addTaskDialogFragment = new AddTaskDialogFragment();
        addTaskDialogFragment.show(fragmentManager, "AddTaskDialogFragment");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void generateListView() {
        for (Integer i = 0; i < 6; i++)
            tasksArrayList.add(String.format("%s %d", getString(R.string.value_examples), i + 1));
    }

    public void setArrayListToListView(ArrayList<String> arrayList, Integer listViewId) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_checked, arrayList);
        ((ListView) findViewById(listViewId)).setAdapter(arrayAdapter);
    }

    public void deleteCheckedTasks(MenuItem menuItem) {
        ListView tasksListView = findViewById(R.id.listView);
        ArrayList<Integer> checkedTasks = new ArrayList<>();
        SparseBooleanArray checkedTasksPosition = tasksListView.getCheckedItemPositions();

        for (Integer i=0; i<tasksListView.getCount(); i++)
            if(checkedTasksPosition.get(i))
                checkedTasks.add(i);

        for (int i=checkedTasks.size()-1; i>=0 ; i--)
            tasksArrayList.remove((int)checkedTasks.get(i));

        setArrayListToListView(tasksArrayList, R.id.listView);
    }

    public void saveTasks(MenuItem menuItem) {
        Boolean ifFileWasSaved = fileManager.saveFile(tasksArrayList);
        if (ifFileWasSaved)
            Toast.makeText(this, R.string.file_was_saved, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, R.string.file_was_not_saved, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDataPass(String data) {
        if (!data.isEmpty()) {
            tasksArrayList.add(data);
            setArrayListToListView(tasksArrayList, R.id.listView);
        }
    }
}