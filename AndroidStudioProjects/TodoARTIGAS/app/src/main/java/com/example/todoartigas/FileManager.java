package com.example.todoartigas;

import android.content.Context;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class FileManager {
    private Context context;
    private String filename;

    public FileManager(Context context, String filename) {
        this.context = context;
        this.filename = filename;
    }

    public ArrayList<String> readFile() {
        ArrayList<String> tasksArray = new ArrayList<>();
        try {
            FileInputStream fileInputStream = context.openFileInput(filename);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream, StandardCharsets.UTF_8));
            for (String task; (task = bufferedReader.readLine()) != null ;)
                tasksArray.add(task);

            bufferedReader.close();
            fileInputStream.close();
        }

        // The FileNotFoundException must be separated to create a file if not exists.
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        catch (Exception e) {
            e.printStackTrace();
        }

        return tasksArray;
    }

    public Boolean saveFile(ArrayList<String> tasksArray) {
        try {
            FileOutputStream fileOutputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);

            for (Integer i=0; i<tasksArray.size(); i++)
                outputStreamWriter.append(String.format("%s \n", tasksArray.get(i)));

            outputStreamWriter.close();
            fileOutputStream.close();
            return true;
        }

        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
