package com.example.testimplicite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void actionButton(View myVue) {
        switch (myVue.getId()) {
            case R.id.button_SMS: {
                EditText inputNumber = (EditText) findViewById(R.id.editTextNumber);
                if (inputNumber.getText().toString() != null && !inputNumber.getText().toString().trim().isEmpty()) {
                    Uri sms = Uri.parse("sms:" + inputNumber.getText().toString());
                    Intent intent = new Intent(Intent.ACTION_SENDTO, sms);
                    if (intent.resolveActivity(getPackageManager()) != null)
                        startActivity(intent);
                    else
                        Toast.makeText(this, R.string.app_not_found, Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(this, R.string.no_input, Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.button_MMS: {
                EditText inputNumber = (EditText) findViewById(R.id.editTextNumber);
                if (inputNumber.getText().toString() != null && !inputNumber.getText().toString().trim().isEmpty()) {
                    Uri mms = Uri.parse("mms:" + inputNumber.getText().toString());
                    Intent intent = new Intent(Intent.ACTION_SENDTO, mms);
                    if (intent.resolveActivity(getPackageManager()) != null)
                        startActivity(intent);
                    else
                        Toast.makeText(this, R.string.app_not_found, Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(this, R.string.no_input, Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.button_Call: {
                EditText inputNumber = (EditText) findViewById(R.id.editTextNumber);
                if (inputNumber.getText().toString() != null && !inputNumber.getText().toString().trim().isEmpty()) {
                    Uri tel = Uri.parse("tel:" + inputNumber.getText().toString());
                    Intent intent = new Intent(Intent.ACTION_DIAL, tel);
                    if (intent.resolveActivity(getPackageManager()) != null)
                        startActivity(intent);
                    else
                        Toast.makeText(this, R.string.app_not_found, Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(this, R.string.no_input, Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.button_Web: {
                EditText inputURL = (EditText) findViewById(R.id.editTextURL);
                if (inputURL.getText().toString() != null && !inputURL.getText().toString().trim().isEmpty()) {
                    Uri url = Uri.parse(inputURL.getText().toString());
                    Intent intent = new Intent(Intent.ACTION_VIEW, url);
                    if (intent.resolveActivity(getPackageManager()) != null)
                        startActivity(intent);
                    else
                        Toast.makeText(this, R.string.app_not_found, Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(this, R.string.no_input, Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.button_Map: {
                EditText inputLatitude = (EditText) findViewById(R.id.editTextLatitude);
                EditText inputLongitude = (EditText) findViewById(R.id.editTextLongitude);
                if (    inputLatitude.getText().toString() != null && !inputLatitude.getText().toString().trim().isEmpty() &&
                        inputLongitude.getText().toString() != null && !inputLongitude.getText().toString().trim().isEmpty()) {
                    if (latitudeLongitudeValidation(Double.parseDouble(inputLatitude.getText().toString()), Double.parseDouble(inputLongitude.getText().toString()))) {
                        Uri url = Uri.parse("geo:" + Double.parseDouble(inputLatitude.getText().toString()) + Double.parseDouble(inputLongitude.getText().toString()));
                        Toast.makeText(this, "geo:" + Double.parseDouble(inputLatitude.getText().toString()) + "," + Double.parseDouble(inputLongitude.getText().toString()), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Intent.ACTION_VIEW, url);
                        if (intent.resolveActivity(getPackageManager()) != null)
                            startActivity(intent);
                        else
                            Toast.makeText(this, R.string.app_not_found, Toast.LENGTH_SHORT).show();
                    }
                    else
                        Toast.makeText(this, R.string.latitude_longitude_not_correct, Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(this, R.string.no_input, Toast.LENGTH_SHORT).show();
                break;
            }
        }
        Button buttonPressed = (Button) findViewById(myVue.getId());
        System.out.println(buttonPressed.getText());
    }

    public Boolean latitudeLongitudeValidation(Double latitude, Double longitude) {
        return (latitude >= -90 && latitude <= 90 && longitude >= -180 && longitude <= 180);
    }
}