package com.example.tpimagetoto;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity {
    public static final int UPLOAD_PICTURE = 1;
    public static final String IMAGE_URI_KEY = "imageURI";

    private Uri imageURIGlobal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            imageURIGlobal = (Uri) savedInstanceState.getParcelable(IMAGE_URI_KEY);
            getImageFromURI(imageURIGlobal);
        }
    }

    public void upload(View myVue) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivityForResult(intent, UPLOAD_PICTURE);
        else
            Toast.makeText(this, R.string.app_not_found, Toast.LENGTH_SHORT).show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UPLOAD_PICTURE)
            if (resultCode != Activity.RESULT_CANCELED) {
                imageURIGlobal = data.getData();
                getImageFromURI(imageURIGlobal);
            }
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(IMAGE_URI_KEY, imageURIGlobal);
        super.onSaveInstanceState(outState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        imageURIGlobal = savedInstanceState.getParcelable(IMAGE_URI_KEY);
    }

    private void getImageFromURI(Uri imageURI) {
        BitmapFactory.Options option = new BitmapFactory.Options();
        option.inMutable = true;
        try {
            Bitmap bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageURI), null, option);
            ImageView imageView = (ImageView) findViewById(R.id.image_view);
            imageView.setImageBitmap(bm);
            TextView textView = (TextView) findViewById(R.id.text_uri);
            textView.setText(imageURI.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void horizontalMirror(View myVue) {
        ImageView imageView = (ImageView) findViewById(R.id.image_view);
        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        Toast.makeText(this, R.string.inverting_picture, Toast.LENGTH_LONG).show();
        for (Integer i=0; i<bitmap.getHeight(); i++) {
            for (Integer j=0; j<bitmap.getWidth() / 2; j++) {
                Integer pixel = bitmap.getPixel(j, i);
                bitmap.setPixel(j, i, bitmap.getPixel(bitmap.getWidth() - j - 1, i));
                bitmap.setPixel(bitmap.getWidth() - j - 1, i, pixel);
            }
        }
        imageView.setImageBitmap(bitmap);
        Toast.makeText(this, R.string.finished_inverting_picture, Toast.LENGTH_SHORT).show();
    }

    public void verticalMirror(View myVue) {
        ImageView imageView = (ImageView) findViewById(R.id.image_view);
        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        Toast.makeText(this, R.string.inverting_picture, Toast.LENGTH_LONG).show();
        for (Integer i=0; i<bitmap.getHeight() / 2; i++) {
            for (Integer j=0; j<bitmap.getWidth(); j++) {
                Integer pixel = bitmap.getPixel(j, i);
                bitmap.setPixel(j, i, bitmap.getPixel(j, bitmap.getHeight() - i - 1));
                bitmap.setPixel(j, bitmap.getHeight() - i - 1, pixel);
            }
        }
        imageView.setImageBitmap(bitmap);
        Toast.makeText(this, R.string.finished_inverting_picture, Toast.LENGTH_SHORT).show();
    }
}