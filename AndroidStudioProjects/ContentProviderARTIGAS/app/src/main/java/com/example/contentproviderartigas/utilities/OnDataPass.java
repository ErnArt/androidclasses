package com.example.contentproviderartigas.utilities;

public interface OnDataPass<T> {
    public void onDataPass(T data, Integer action);
}
