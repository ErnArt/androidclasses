package com.example.contentproviderartigas.arrayadapters;

import android.content.Context;

import com.example.contentproviderartigas.utilities.Contact;

import java.util.ArrayList;

public class ContactArrayAdapter extends TwoLineArrayAdapter<Contact> {
    public ContactArrayAdapter(Context context, ArrayList<Contact> contacts) {
        super(context, contacts);
    }

    @Override
    public String lineOneText(Contact contact) {
        return contact.name;
    }

    @Override
    public String lineTwoText(Contact contact) {
        return contact.number;
    }
}
