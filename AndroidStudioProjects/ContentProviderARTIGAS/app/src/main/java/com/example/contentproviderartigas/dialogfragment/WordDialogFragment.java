package com.example.contentproviderartigas.dialogfragment;

import android.widget.EditText;

import com.example.contentproviderartigas.R;

public class WordDialogFragment extends InputDialogFragment<String> {
    public WordDialogFragment(Integer layoutID, Integer action) {
        super(layoutID, action);
    }

    @Override
    public void submitInput() {
        passData(((EditText) getDialog().findViewById(R.id.taskEditText)).getText().toString().trim());
    }

    @Override
    public void passData(String data) {
        dataPass.onDataPass(data, action);
    }
}
