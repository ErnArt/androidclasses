package com.example.contentproviderartigas.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import com.example.contentproviderartigas.R;
import com.example.contentproviderartigas.dialogfragment.WordDialogFragment;
import com.example.contentproviderartigas.utilities.OnDataPass;

import java.util.ArrayList;
import java.util.Locale;

public class WordActivity extends AppCompatActivity implements OnDataPass<String> {
    public static final int ADD_ITEM = 1;
    public static final int DELETE_ITEM = 2;

    public static final String WORD_ARRAY_LIST_KEY = "wordArrayListKey";
    public static final String ORDER_SWITCH_VALUE_KEY = "orderSwitchValueKey";

    public ArrayList<String> wordArrayList;
    public ArrayAdapter<String> arrayAdapter;
    public String orderSwitchValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word);

        if (savedInstanceState != null) {
            wordArrayList = savedInstanceState.getStringArrayList(WORD_ARRAY_LIST_KEY);
            orderSwitchValue = savedInstanceState.getString(ORDER_SWITCH_VALUE_KEY);
            Boolean isChecked = (orderSwitchValue == getString(R.string.order_switch_on)) ?
                    true :
                    false;
            ((Switch) findViewById(R.id.orderSwitch)).setChecked(isChecked);
        }

        else {
            wordArrayList = new ArrayList<>();
            orderSwitchValue = (String) getText(R.string.order_switch_off);
        }


        ((Switch) findViewById(R.id.orderSwitch)).setText(orderSwitchValue);
        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, wordArrayList);

        ((ListView) findViewById(R.id.listView)).setAdapter(arrayAdapter);
        getListFromContentResolver(null);
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putStringArrayList(WORD_ARRAY_LIST_KEY, wordArrayList);
        outState.putString(ORDER_SWITCH_VALUE_KEY, orderSwitchValue);
        super.onSaveInstanceState(outState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        wordArrayList = savedInstanceState.getStringArrayList(WORD_ARRAY_LIST_KEY);
        orderSwitchValue = savedInstanceState.getString(ORDER_SWITCH_VALUE_KEY);
    }

    public void onSwitchClicked(View myVue) {
        orderSwitchValue = (String) ((((Switch) findViewById(R.id.orderSwitch)).isChecked()) ? getText(R.string.order_switch_on) : getText(R.string.order_switch_off));
        ((Switch) findViewById(R.id.orderSwitch)).setText(orderSwitchValue);
        getListFromContentResolver(null);
    }

    public void refreshArrayAdapter() {
        arrayAdapter.notifyDataSetChanged();
    }

    public void getListFromContentResolver(View myVue) {
        wordArrayList.clear();

        ContentResolver resolver = getContentResolver();
        Cursor cursor = resolver.query(
            UserDictionary.Words.CONTENT_URI,
            null,
            null,
            null,
            String.format("%s %s", UserDictionary.Words.WORD, (orderSwitchValue == getString(R.string.order_switch_on)) ? "ASC" : "DESC"),
            null
        );


        Integer index = cursor.getColumnIndex(UserDictionary.Words.WORD);
        while(cursor.moveToNext())
            wordArrayList.add(cursor.getString(index));

        refreshArrayAdapter();
    }

    public void addWordToList(View myVue) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        WordDialogFragment wordDialogFragment = new WordDialogFragment(R.layout.activity_dialog_fragment_input, WordActivity.ADD_ITEM);
        wordDialogFragment.show(fragmentManager, "WordDialogFragment");
    }

    public void deleteWordFromList(View myVue) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        WordDialogFragment wordDialogFragment = new WordDialogFragment(R.layout.activity_dialog_fragment_input, WordActivity.DELETE_ITEM);
        wordDialogFragment.show(fragmentManager, "WordDialogFragment");
    }

    public boolean doesWordExist (String word) {
        for (String element : wordArrayList)
            if (element.equals(word))
                return true;
        return false;
    }

    @Override
    public void onDataPass(String data, Integer action) {
        if (!data.isEmpty()) {
            ContentResolver resolver = getContentResolver();
            switch (action) {
                case ADD_ITEM: {
                    String result = "";

                    if (doesWordExist(data))
                        result = String.format("%s %s", data, getString(R.string.item_already_exists));

                    else {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(UserDictionary.Words.LOCALE, (Locale.getDefault()).toString());
                        contentValues.put(UserDictionary.Words.WORD, data);
                        contentValues.put(UserDictionary.Words.FREQUENCY, 250);

                        resolver.insert(UserDictionary.Words.CONTENT_URI, contentValues);
                        result = String.format("%s %s", data, getString(R.string.item_added));
                    }


                    Toast.makeText(this, result, Toast.LENGTH_SHORT).show();

                    getListFromContentResolver(null);
                    break;
                }

                case DELETE_ITEM: {
                    String result = "";
                    if (!doesWordExist(data))
                        result = String.format("%s %s", data, getString(R.string.item_not_exist));

                    else {
                        resolver.delete(
                            UserDictionary.Words.CONTENT_URI,
                            String.format("%s LIKE ?", UserDictionary.Words.WORD),
                            new String[] {data}
                        );

                        result = String.format("%s %s", data, getString(R.string.item_deleted));
                    }

                    Toast.makeText(this, result, Toast.LENGTH_SHORT).show();

                    getListFromContentResolver(null);
                    break;
                }
            }
        }
    }
}