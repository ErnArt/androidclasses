package com.example.contentproviderartigas.dialogfragment;

import android.app.AlertDialog;
import android.app.Dialog;
import androidx.fragment.app.DialogFragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;

import com.example.contentproviderartigas.R;
import com.example.contentproviderartigas.utilities.OnDataPass;

public abstract class InputDialogFragment<T> extends DialogFragment {
    OnDataPass dataPass;
    Integer layoutID;
    Integer action;

    public InputDialogFragment(Integer layoutID, Integer action) {
        this.layoutID = layoutID;
        this.action = action;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        builder .setView(inflater.inflate(layoutID, null))
                .setMessage(R.string.task_title_input_activity)
                .setPositiveButton(R.string.button_submit, (dialog, which) -> submitInput())
                .setNegativeButton(R.string.button_cancel, (dialog, which) -> dismiss());

        return builder.create();
    }

    public abstract void submitInput();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPass = (OnDataPass) context;
    }

    public abstract void passData(T data);
}
