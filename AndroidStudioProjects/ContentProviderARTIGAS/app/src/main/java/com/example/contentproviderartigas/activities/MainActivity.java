package com.example.contentproviderartigas.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.contentproviderartigas.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void launchActivity(View myVue) {
        switch (myVue.getId()) {
            case R.id.buttonWord: {
                Intent intent = new Intent(this, WordActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.buttonContact: {
                Intent intent = new Intent(this, ContactActivity.class);
                startActivity(intent);
                break;
            }
        }

    }
}