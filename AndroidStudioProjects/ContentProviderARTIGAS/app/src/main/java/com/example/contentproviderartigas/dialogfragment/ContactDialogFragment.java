package com.example.contentproviderartigas.dialogfragment;

import android.widget.EditText;

import com.example.contentproviderartigas.R;
import com.example.contentproviderartigas.utilities.Contact;

public class ContactDialogFragment extends InputDialogFragment<Contact> {
    public ContactDialogFragment(Integer layoutID, Integer action) {
        super(layoutID, action);
    }

    @Override
    public void submitInput() {
        passData(new Contact(
                ((EditText) getDialog().findViewById(R.id.taskEditTextName)).getText().toString().trim(),
                ((EditText) getDialog().findViewById(R.id.taskEditTextPhone)).getText().toString().trim()
        ));
    }

    @Override
    public void passData(Contact data) {
        dataPass.onDataPass(data, action);
    }
}
