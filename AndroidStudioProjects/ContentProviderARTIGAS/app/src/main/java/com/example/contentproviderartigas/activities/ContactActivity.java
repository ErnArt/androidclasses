package com.example.contentproviderartigas.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.UserDictionary;
import android.view.View;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import com.example.contentproviderartigas.R;
import com.example.contentproviderartigas.dialogfragment.ContactDialogFragment;
import com.example.contentproviderartigas.utilities.Contact;
import com.example.contentproviderartigas.arrayadapters.ContactArrayAdapter;
import com.example.contentproviderartigas.utilities.OnDataPass;

import java.util.ArrayList;

public class ContactActivity extends AppCompatActivity implements OnDataPass<Contact> {
    public static final int ADD_ITEM = 1;
    public static final int DELETE_ITEM = 2;

    public static final String CONTACT_ARRAY_LIST_KEY = "contactArrayListKey";
    public static final String ORDER_SWITCH_VALUE_KEY = "orderSwitchValueKey";

    public ArrayList<Contact> contactArrayList;
    public ContactArrayAdapter arrayAdapter;
    public String orderSwitchValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        if (savedInstanceState != null) {
            contactArrayList = savedInstanceState.getParcelableArrayList(CONTACT_ARRAY_LIST_KEY);
            orderSwitchValue = savedInstanceState.getString(ORDER_SWITCH_VALUE_KEY);
            Boolean isChecked = (orderSwitchValue == getString(R.string.order_switch_on)) ?
                    true :
                    false;
            ((Switch) findViewById(R.id.orderSwitch)).setChecked(isChecked);
        }

        else {
            contactArrayList = new ArrayList<>();
            orderSwitchValue = (String) getText(R.string.order_switch_off);
        }

        ((Switch) findViewById(R.id.orderSwitch)).setText(orderSwitchValue);
        arrayAdapter = new ContactArrayAdapter(this, contactArrayList);

        ((ListView) findViewById(R.id.listView)).setAdapter(arrayAdapter);
        getListFromContentResolver(null);
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(CONTACT_ARRAY_LIST_KEY, contactArrayList);
        outState.putString(ORDER_SWITCH_VALUE_KEY, orderSwitchValue);
        super.onSaveInstanceState(outState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        contactArrayList = savedInstanceState.getParcelableArrayList(CONTACT_ARRAY_LIST_KEY);
        orderSwitchValue = savedInstanceState.getString(ORDER_SWITCH_VALUE_KEY);
    }

    public void onSwitchClicked(View myVue) {
        orderSwitchValue = (String) ((((Switch) findViewById(R.id.orderSwitch)).isChecked()) ? getText(R.string.order_switch_on) : getText(R.string.order_switch_off));
        ((Switch) findViewById(R.id.orderSwitch)).setText(orderSwitchValue);
        getListFromContentResolver(null);
    }

    public void refreshArrayAdapter() {
        arrayAdapter.notifyDataSetChanged();
    }

    @SuppressLint("Range")
    public void getListFromContentResolver(View myVue) {
        contactArrayList.clear();

        ContentResolver resolver = getContentResolver();
        Cursor cursor = resolver.query(
            ContactsContract.Contacts.CONTENT_URI,
            null,
            null,
            null,
            String.format("%s %s", ContactsContract.Contacts.DISPLAY_NAME,
                    (orderSwitchValue == getString(R.string.order_switch_on)) ? "ASC" : "DESC")
        );

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {

                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor cursorInfo = resolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        String.format("%s = ?", ContactsContract.CommonDataKinds.Phone.CONTACT_ID),
                        new String[] {id},
                        null
                    );

                    cursorInfo.moveToNext();

                    contactArrayList.add(new Contact(
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)),
                        cursorInfo.getString(cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)))
                    );
                }
            }
            refreshArrayAdapter();
        }
    }

    public void addContactToList(View myVue) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        ContactDialogFragment contactDialogFragment = new ContactDialogFragment(R.layout.activity_dialog_fragment_two_item_input, ContactActivity.ADD_ITEM);
        contactDialogFragment.show(fragmentManager, "ContactDialogFragment");
    }

    public void deleteContactFromList(View myVue) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        ContactDialogFragment contactDialogFragment = new ContactDialogFragment(R.layout.activity_dialog_fragment_two_item_input, ContactActivity.DELETE_ITEM);
        contactDialogFragment.show(fragmentManager, "ContactDialogFragment");
    }

    @SuppressLint("Range")
    public boolean doesContactExist (Contact contact) {
        for (Contact element : contactArrayList)
            if (element.name.equals(contact.name) && element.number.equals(contact.number))
                return true;
        return false;
    }

    @Override
    public void onDataPass(Contact data, Integer action) {
        if (!data.number.isEmpty() || !data.name.isEmpty()) {
            ContentResolver resolver = getContentResolver();
            switch (action) {
                case ADD_ITEM: {
                    String result = "";
                    
                    if (doesContactExist(data))
                        result = String.format("%s %s", data.name, getString(R.string.item_already_exists));

                    else {
                        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
                        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                                .build()
                        );

                        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, data.name)
                                .build()
                        );


                        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                                .withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, data.number)
                                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                                .build()
                        );

                        try {
                            resolver.applyBatch(ContactsContract.AUTHORITY, ops);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        result = String.format("%s %s", data.name, getString(R.string.item_added));
                    }

                    Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
                    getListFromContentResolver(null);
                    break;
                }

                case DELETE_ITEM: {
                    String result = "";

                    if (!doesContactExist(data))
                        result = String.format("%s %s", data.name, getString(R.string.item_not_exist));

                    else {
                        resolver.delete(
                            ContactsContract.RawContacts.CONTENT_URI,
                            String.format("%s LIKE ?", ContactsContract.Contacts.DISPLAY_NAME),
                            new String[] {data.name}
                        );

                        result = String.format("%s %s", data.name, getString(R.string.item_deleted));
                    }

                    Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
                    getListFromContentResolver(null);
                    break;
                }
            }
        }
    }
}