package com.example.tpimagetoto;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity {
    public static final int UPLOAD_PICTURE = 1;
    public static final String IMAGE_URI_KEY = "imageURI";

    private Uri imageURIGlobal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerForContextMenu((ImageView) findViewById(R.id.image_view));

        // Verify if there was a picture, then taking it from the bundle and if not null load it.
        if (savedInstanceState == null)
            return;

        imageURIGlobal = (Uri) savedInstanceState.getParcelable(IMAGE_URI_KEY);

        if (imageURIGlobal == null)
            return;

        getImageFromURI(imageURIGlobal);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        // To avoid to have a contextual menu on a blank picture and crash the app.
        if (((ImageView) findViewById(R.id.image_view)).getDrawable() != null) {
            super.onCreateContextMenu(menu, v, menuInfo);
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_image_view, menu);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UPLOAD_PICTURE && resultCode != Activity.RESULT_CANCELED) {
            imageURIGlobal = data.getData();
            getImageFromURI(imageURIGlobal);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(IMAGE_URI_KEY, imageURIGlobal);
        super.onSaveInstanceState(outState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        imageURIGlobal = savedInstanceState.getParcelable(IMAGE_URI_KEY);
    }

    public void upload(View myVue) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivityForResult(intent, UPLOAD_PICTURE);
        else
            Toast.makeText(this, R.string.app_not_found, Toast.LENGTH_SHORT).show();
    }

    private void getImageFromURI(Uri imageURI) {
        BitmapFactory.Options option = new BitmapFactory.Options();
        option.inMutable = true;
        try {
            Bitmap bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageURI), null, option);
            ((ImageView) findViewById(R.id.image_view)).setImageBitmap(bm);
            ((TextView) findViewById(R.id.text_uri)).setText(imageURI.toString());
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void itemRestore(View myVue) {
        ImageView imageView = getImageViewIfNotNull();
        if (imageView == null) return;

        getImageFromURI(imageURIGlobal);
    }

    @Nullable
    private ImageView getImageViewIfNotNull() {
        ImageView imageView = (ImageView) findViewById(R.id.image_view);
        if (imageView.getDrawable() == null) {
            Toast.makeText(this, R.string.no_picture, Toast.LENGTH_SHORT).show();
            return null;
        }
        return imageView;
    }

    public void mirroring(MenuItem menuItem) {
        ImageView imageView = getImageViewIfNotNull();
        if (imageView == null) return;

        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        Toast.makeText(this, R.string.processing_picture, Toast.LENGTH_SHORT).show();

        switch (menuItem.getItemId()) {
            case R.id.itemMirrorHorizontal: {
                imageView.setImageBitmap(getBitmapHorizontalMirrored(bitmap.getHeight(), bitmap.getWidth(), bitmap));
                break;
            }
            case R.id.itemMirrorVertical: {
                imageView.setImageBitmap(getBitmapVerticalMirrored(bitmap.getHeight(), bitmap.getWidth(), bitmap));
                break;
            }
        }
    }

    public Bitmap getBitmapHorizontalMirrored(Integer height, Integer width, Bitmap bitmap) {
        for (Integer i=0; i<height; i++)
            for (Integer j=0; j<width/2; j++) {
                Integer pixel = bitmap.getPixel(j, i);
                bitmap.setPixel(j, i, bitmap.getPixel(width - j - 1, i));
                bitmap.setPixel(width - j - 1, i, pixel);
            }

        return bitmap;
    }

    public Bitmap getBitmapVerticalMirrored(Integer height, Integer width, Bitmap bitmap) {
        for (Integer i=0; i<height/2; i++)
            for (Integer j=0; j<width; j++) {
                Integer pixel = bitmap.getPixel(j, i);
                bitmap.setPixel(j, i, bitmap.getPixel(j, height - i - 1));
                bitmap.setPixel(j, height - i - 1, pixel);
            }

        return bitmap;
    }

    public void rotation(MenuItem menuItem) {
        ImageView imageView = getImageViewIfNotNull();
        if (imageView == null) return;

        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        Toast.makeText(this, R.string.processing_picture, Toast.LENGTH_SHORT).show();

        switch (menuItem.getItemId()) {
            case R.id.itemClockwiseRotation: {
                imageView.setImageBitmap(getBitmapRotated(-90, bitmap));
                break;
            }
            case R.id.itemCounterClockwiseRotation: {
                imageView.setImageBitmap(getBitmapRotated(90, bitmap));
                break;
            }
        }
    }

    public Bitmap getBitmapRotated(Integer degree, Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        return Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public Integer calculatePercentage(Integer value, Integer percentage) {
        return percentage * value / 100;
    }

    public void colorAlteration(MenuItem menuItem) {
        ImageView imageView = getImageViewIfNotNull();
        if (imageView == null) return;

        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        Toast.makeText(this, R.string.processing_picture, Toast.LENGTH_SHORT).show();

        switch (menuItem.getItemId()) {
            case R.id.itemInvertColor: {
                imageView.setImageBitmap(getBitmapInvertedColor(bitmap.getHeight(), bitmap.getWidth(), bitmap));
                break;
            }
            case R.id.itemGreyScale: {
                imageView.setImageBitmap(getBitmapGreyScale(bitmap.getHeight(), bitmap.getWidth(), bitmap));
                break;
            }
            case R.id.itemGreyScale2: {
                imageView.setImageBitmap(getBitmapGreyScale2(bitmap.getHeight(), bitmap.getWidth(), bitmap));
                break;
            }
            case R.id.itemGreyScale3: {
                imageView.setImageBitmap(getBitmapGreyScale3(bitmap.getHeight(), bitmap.getWidth(), bitmap));
                break;
            }
        }
    }

    public Bitmap getBitmapInvertedColor(Integer height, Integer width, Bitmap bitmap) {
        for (Integer i=0; i<height; i++)
            for (Integer j=0; j<width; j++) {
                Integer pixel = bitmap.getPixel(j, i);
                bitmap.setPixel(j, i, Color.rgb(255 - Color.red(pixel), 255 - Color.green(pixel), 255 - Color.blue(pixel)));
            }

        return bitmap;
    }

    public Bitmap getBitmapGreyScale(Integer height, Integer width, Bitmap bitmap) {
        for (Integer i=0; i<height; i++)
            for (Integer j=0; j<width; j++) {
                Integer pixel = bitmap.getPixel(j, i);
                Integer averageValue = (Color.red(pixel) + Color.green(pixel) + Color.blue(pixel))/3;
                bitmap.setPixel(j, i, Color.rgb(averageValue, averageValue, averageValue));
            }

        return bitmap;
    }

    public Bitmap getBitmapGreyScale2(Integer height, Integer width, Bitmap bitmap) {
        for (Integer i=0; i<height; i++)
            for (Integer j=0; j<width; j++) {
                Integer pixel = bitmap.getPixel(j, i);
                Integer greyValue = (Math.max(Color.red(pixel), Math.max(Color.green(pixel), Color.blue(pixel)))
                    + Math.max(Color.red(pixel), Math.max(Color.green(pixel), Color.blue(pixel))))/2;
                bitmap.setPixel(j, i, Color.rgb(greyValue, greyValue, greyValue));
            }

        return bitmap;
    }

    public Bitmap getBitmapGreyScale3(Integer height, Integer width, Bitmap bitmap) {
        for (Integer i=0; i<height; i++)
            for (Integer j=0; j<width; j++) {
                Integer pixel = bitmap.getPixel(j, i);
                Integer greyValue = calculatePercentage(Color.red(pixel), 21) +
                                    calculatePercentage(Color.green(pixel), 72) +
                                    calculatePercentage(Color.blue(pixel), 7);
                bitmap.setPixel(j, i, Color.rgb(greyValue, greyValue, greyValue));
            }

        return bitmap;
    }



}