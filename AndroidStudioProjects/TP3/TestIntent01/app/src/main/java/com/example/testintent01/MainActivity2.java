package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intention = getIntent();
        String inputText = intention.getStringExtra(MainActivity.INPUTTEXT).trim();
        TextView resultText = (TextView) findViewById(R.id.textView);
        if (TextUtils.isEmpty(inputText)) {
            resultText.setText(R.string.text_view_activity2);
        }
        else {
            resultText.setText(inputText);
        }
    }
}