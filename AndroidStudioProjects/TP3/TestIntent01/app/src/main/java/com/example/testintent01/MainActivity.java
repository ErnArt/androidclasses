package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public final static String INPUTTEXT = "com.example.textintent01.INPUTTEXT";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void printText(View myVue) {
        Intent intention = new Intent(this, MainActivity2.class);
        EditText inputText1 = (EditText) findViewById(R.id.inputText1);
        String textFromInput = inputText1.getText().toString();
        intention.putExtra(INPUTTEXT, textFromInput);
        startActivity(intention);
    }
}