package com.example.testintent02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {
    public final static String INPUTTED_TEXT = "com.example.textintent02.INPUTTED_TEXT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        String existingText = intent.getStringExtra(MainActivity.EXISTING_TEXT).trim();
        EditText editText1 = (EditText) findViewById(R.id.editTextTextPersonName);
        editText1.setText(existingText);
    }

    public void inputText(View myVue) {
        Intent intent = getIntent();
        String existingText = intent.getStringExtra(MainActivity.EXISTING_TEXT).trim();
        Intent result = new Intent();
        EditText editText1 = (EditText) findViewById(R.id.editTextTextPersonName);
        String inputtedText = editText1.getText().toString();

        if (TextUtils.isEmpty(inputtedText)) {
            result.putExtra(INPUTTED_TEXT, existingText);
            setResult(RESULT_CANCELED, result);
        }

        else {
            result.putExtra(INPUTTED_TEXT, inputtedText);
            setResult(RESULT_OK, result);
        }

        finish();
    }
}