package com.example.testintent02;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final int CHANGE_TEXT = 1;
    public final static String EXISTING_TEXT = "com.example.textintent02.EXISTING_TEXT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void changeText(View myVue) {
        Intent intention = new Intent(this, MainActivity2.class);
        TextView textViewActivity1 = (TextView) findViewById(R.id.textViewActivity1);
        String textFromInput = textViewActivity1.getText().toString();
        intention.putExtra(EXISTING_TEXT, textFromInput);
        startActivityForResult(intention, CHANGE_TEXT);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHANGE_TEXT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, R.string.error_text_input, Toast.LENGTH_SHORT).show();
            }
            else {
                String inputtedText = data.getStringExtra(MainActivity2.INPUTTED_TEXT).trim();
                TextView textViewActivity1 = (TextView) findViewById(R.id.textViewActivity1);
                textViewActivity1.setText(inputtedText);
            }
        }
    }
}