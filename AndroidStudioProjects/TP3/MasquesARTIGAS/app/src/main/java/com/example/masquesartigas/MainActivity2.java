package com.example.masquesartigas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.EditText;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        EditText firstNameInput = (EditText) findViewById(R.id.firstNameInput);
        EditText surnameInput = (EditText) findViewById(R.id.surnameInput);
        EditText phoneNumberInput = (EditText) findViewById(R.id.phoneNumberInput);
        firstNameInput.setText(intent.getStringExtra(MainActivity.FIRST_NAME_TEXT).trim());
        surnameInput.setText(intent.getStringExtra(MainActivity.SURNAME_TEXT).trim());
        phoneNumberInput.setText(intent.getStringExtra(MainActivity.PHONE_NUMBER_TEXT).trim());
    }

    public void submitInput(View myVue) {
        Intent result = new Intent();
        EditText firstNameInput = (EditText) findViewById(R.id.firstNameInput);
        EditText surnameInput = (EditText) findViewById(R.id.surnameInput);
        EditText phoneNumberInput = (EditText) findViewById(R.id.phoneNumberInput);
        result.putExtra(MainActivity.FIRST_NAME_TEXT, firstNameInput.getText().toString().trim());
        result.putExtra(MainActivity.SURNAME_TEXT, surnameInput.getText().toString().trim());
        result.putExtra(MainActivity.PHONE_NUMBER_TEXT, phoneNumberInput.getText().toString().trim());
        setResult(RESULT_OK, result);
        finish();
    }

    public void cancelInput(View myVue) {
        Intent result = new Intent();
        setResult(RESULT_CANCELED, result);
        finish();
    }
}