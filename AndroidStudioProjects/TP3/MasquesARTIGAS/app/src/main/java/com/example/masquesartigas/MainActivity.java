package com.example.masquesartigas;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final int CHANGE_USER = 1;
    public static final int CHANGE_ADDRESS = 2;
    public static final String FIRST_NAME_TEXT = "com.example.masquesartigas.FIRST_NAME_TEXT";
    public static final String SURNAME_TEXT = "com.example.masquesartigas.SURNAME_TEXT";
    public static final String PHONE_NUMBER_TEXT = "com.example.masquesartigas.PHONE_NUMBER_TEXT";
    public static final String ADDRESS_NUMBER = "com.example.masquesartigas.ADDRESS_NUMBER";
    public static final String ADDRESS_NAME = "com.example.masquesartigas.ADDRESS_NAME";
    public static final String ADDRESS_CODE = "com.example.masquesartigas.ADDRESS_CODE";
    public static final String ADDRESS_CITY = "com.example.masquesartigas.ADDRESS_CITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void changeUserInfos(View myVue) {
        Intent intent = new Intent(this, MainActivity2.class);
        TextView firstNameView = (TextView) findViewById(R.id.firstNameView);
        TextView surnameView = (TextView) findViewById(R.id.surnameView);
        TextView phoneNumberView = (TextView) findViewById(R.id.phoneNumberView);
        intent.putExtra(FIRST_NAME_TEXT, firstNameView.getText().toString());
        intent.putExtra(SURNAME_TEXT, surnameView.getText().toString());
        intent.putExtra(PHONE_NUMBER_TEXT, phoneNumberView.getText().toString());
        startActivityForResult(intent, CHANGE_USER);
    }

    public void changeAddressInfos(View myVue) {
        Intent intent = new Intent(this, MainActivity3.class);
        TextView addressNumberView = (TextView) findViewById(R.id.addressNumberView);
        TextView addressNameView = (TextView) findViewById(R.id.addressNameView);
        TextView addressCodeView = (TextView) findViewById(R.id.addressCodeView);
        TextView addressCityView = (TextView) findViewById(R.id.addressCityView);
        intent.putExtra(ADDRESS_NUMBER, addressNumberView.getText().toString());
        intent.putExtra(ADDRESS_NAME, addressNameView.getText().toString());
        intent.putExtra(ADDRESS_CODE, addressCodeView.getText().toString());
        intent.putExtra(ADDRESS_CITY, addressCityView.getText().toString());
        startActivityForResult(intent, CHANGE_ADDRESS);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHANGE_USER) {
            if (resultCode != Activity.RESULT_CANCELED) {
                TextView firstNameView = (TextView) findViewById(R.id.firstNameView);
                TextView surnameView = (TextView) findViewById(R.id.surnameView);
                TextView phoneNumberView = (TextView) findViewById(R.id.phoneNumberView);
                firstNameView.setText(data.getStringExtra(FIRST_NAME_TEXT).trim());
                surnameView.setText(data.getStringExtra(SURNAME_TEXT).trim());
                phoneNumberView.setText(data.getStringExtra(PHONE_NUMBER_TEXT).trim());
            }
        }
        else if (requestCode == CHANGE_ADDRESS) {
            if (resultCode != Activity.RESULT_CANCELED) {
                TextView addressNumberView = (TextView) findViewById(R.id.addressNumberView);
                TextView addressNameView = (TextView) findViewById(R.id.addressNameView);
                TextView addressCodeView = (TextView) findViewById(R.id.addressCodeView);
                TextView addressCityView = (TextView) findViewById(R.id.addressCityView);
                addressNumberView.setText(data.getStringExtra(ADDRESS_NUMBER).trim());
                addressNameView.setText(data.getStringExtra(ADDRESS_NAME).trim());
                addressCodeView.setText(data.getStringExtra(ADDRESS_CODE).trim());
                addressCityView.setText(data.getStringExtra(ADDRESS_CITY).trim());
            }

        }
    }
}