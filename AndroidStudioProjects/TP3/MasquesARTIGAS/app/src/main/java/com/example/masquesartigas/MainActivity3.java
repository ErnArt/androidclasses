package com.example.masquesartigas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        Intent intent = getIntent();
        EditText addressNumberInput = (EditText) findViewById(R.id.addressNumberInput);
        EditText addressNameInput = (EditText) findViewById(R.id.addressNameInput);
        EditText addressCodeInput = (EditText) findViewById(R.id.addressCodeInput);
        EditText addressCityInput = (EditText) findViewById(R.id.addressCityInput);
        addressNumberInput.setText(intent.getStringExtra(MainActivity.ADDRESS_NUMBER).trim());
        addressNameInput.setText(intent.getStringExtra(MainActivity.ADDRESS_NAME).trim());
        addressCodeInput.setText(intent.getStringExtra(MainActivity.ADDRESS_CODE).trim());
        addressCityInput.setText(intent.getStringExtra(MainActivity.ADDRESS_CITY).trim());
    }

    public void submitInput(View myVue) {
        Intent result = new Intent();
        EditText addressNumberInput = (EditText) findViewById(R.id.addressNumberInput);
        EditText addressNameInput = (EditText) findViewById(R.id.addressNameInput);
        EditText addressCodeInput = (EditText) findViewById(R.id.addressCodeInput);
        EditText addressCityInput = (EditText) findViewById(R.id.addressCityInput);
        result.putExtra(MainActivity.ADDRESS_NUMBER, addressNumberInput.getText().toString().trim());
        result.putExtra(MainActivity.ADDRESS_NAME, addressNameInput.getText().toString().trim());
        result.putExtra(MainActivity.ADDRESS_CODE, addressCodeInput.getText().toString().trim());
        result.putExtra(MainActivity.ADDRESS_CITY, addressCityInput.getText().toString().trim());
        setResult(RESULT_OK, result);
        finish();
    }

    public void cancelInput(View myVue) {
        Intent result = new Intent();
        setResult(RESULT_CANCELED, result);
        finish();
    }
}