package com.example.calcartigas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void resetGUI(View myVue) {
        EditText inputText1 = (EditText) findViewById(R.id.inputText1);
        EditText inputText2 = (EditText) findViewById(R.id.inputText2);
        TextView resultText = (TextView) findViewById(R.id.resultText);
        inputText1.setText("");
        inputText2.setText("");
        resultText.setText(R.string.resultText);
    }

    public void calculateNumbers(View myVue) {
        EditText inputText1 = (EditText) findViewById(R.id.inputText1);
        EditText inputText2 = (EditText) findViewById(R.id.inputText2);
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        if (radioGroup.getCheckedRadioButtonId() == -1) {
            Toast.makeText(this, R.string.noOperatorText, Toast.LENGTH_SHORT).show();
        }
        else {
            Double input1 = Double.parseDouble(inputText1.getText().toString());
            Double input2 = Double.parseDouble(inputText2.getText().toString());
            TextView resultText = (TextView) findViewById(R.id.resultText);
            Double result;
            RadioButton radioButton = (RadioButton) findViewById(radioGroup.getCheckedRadioButtonId());
            // To get the id given in activity_main.xml instead of relying on comparing the text with the strings.xml.
            String buttonName = getResources().getResourceEntryName(radioButton.getId());
            switch (buttonName) {
                case "radioButtonPlus":
                    result = input1+input2;
                    resultText.setText(result.toString());
                    break;
                case "radioButtonMinus":
                    result = input1-input2;
                    resultText.setText(result.toString());
                    break;

                case "radioButtonMultiply":
                    result = input1*input2;
                    resultText.setText(result.toString());

                    break;

                case "radioButtonDivide":
                    if (input2 != 0) {
                        result = input1/input2;
                        resultText.setText(result.toString());
                    }
                    else
                        resultText.setText(R.string.errorMessageDivide);
                    break;
            }
        }
    }

    public void quitApp(View myVue) {
        finish();
        System.exit(0);
    }
}